module Stasiun.State

open System

// TODO: Design it correctly.
type InitState<'content> =
  { Content : 'content }

type DecisionState<'content> =
  { Guard     : bool list
    Content   : obj
    GoToTrue  : Guid
    GoToFalse : Guid }

type OperationState<'coming, 'out> =
  { Operation : 'coming -> 'out
    Content   : 'out
    GoToNext  : Guid }

type EndState<'fin> =
  { Content : 'fin }

type State<'init, 'fin> =
  | Init      of InitState<'init>
  | Decision  of DecisionState<'init>
  | Operation of OperationState<'init, 'fin>
  | End       of EndState<'fin>
