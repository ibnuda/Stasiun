module Stasiun.Types

open System

type GuardType =
  | OK
  | NotOK
  | DontKnow
  with
    static member Default = DontKnow
    member __.ToBool () =
      match __ with
      | OK -> true
      | _  -> false

type Guard =
  { Condition : GuardType }

type State =
  { Id    : Guid
    Name  : string
    ItemId: Guid }

type Transition =
  { Id  : Guid
    Name: string
    From: Guid
    To  : Guid }

type Decision =
  { Id        : Guid
    Name      : string
    Transition: Transition
    Guards    : Guard list }
