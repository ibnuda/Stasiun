module Stasiun.Hekate

(***
  Copyright (c) 2015 Andrew Cherry
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***)

open System

open Aether
open Aether.Operators


let private flip f a b = f b a
let private swap (a, b) = (b, a)

type Node<'key> = 'key
type Edge<'key> = 'key * 'key

type LNode<'key, 'value> = 'key * 'value
type LEdge<'key, 'label> = 'key * 'key * 'label

type Adjecent<'key, 'label> = ('label * 'key) list

type Context<'key, 'value, 'label> =
  Adjecent<'key, 'label> * 'key * 'value * Adjecent<'key, 'label>

let private predecessor : Lens<Context<'key, _, 'label>, Adjecent<'key, 'label>> =
  (fun (pred, _, _, _) -> pred),
  (fun pred (_, key, value, succ) -> (pred, key, value, succ))

let private nodekey : Lens<Context<'key, _, _>, 'key> =
  (fun (_, key, _, _) -> key),
  (fun key (pred, _, value, succ) -> (pred, key, value, succ))

let private successor : Lens<Context<'key, _, 'label>, Adjecent<'key, 'label>> =
  (fun (_, _, _, succ) -> succ),
  (fun succ (pred, key, value, _) -> (pred, key, value, succ))

//==== Rep types and lenses.
type MapAdjecent<'key, 'label> when 'key : comparison =
  Map<'key, 'label>

type MapContext<'key, 'value, 'label> when 'key : comparison =
  MapAdjecent<'key, 'label> * 'value * MapAdjecent<'key, 'label>

type MapGraph<'key, 'value, 'label> when 'key : comparison =
  Map<'key, MapContext<'key, 'value, 'label>>

type Graph<'key, 'value, 'label> when 'key : comparison =
  MapGraph<'key, 'value, 'label>

let private mapPred :
            Lens<MapContext<'key, _, 'label>, MapAdjecent<'key, 'label>> =
  (fun (pred, _, _) -> pred),
  (fun pred (_, value, succ) -> (pred, value, succ))

let private mapSucc :
            Lens<MapContext<'key, _, 'label>, MapAdjecent<'key, 'label>> =
  (fun (_, _, succ) -> succ),
  (fun succ (pred, value, _) -> (pred, value, succ))

//==== From mapping.
let private fromAdjecent<'key, 'label when 'key : comparison> :
            Adjecent<'key, 'label> -> MapAdjecent<'key, 'label> =
  List.map swap >> Map.ofList

let private toAdjecent<'key, 'label when 'key : comparison> :
            MapAdjecent<'key, 'label> -> Adjecent<'key, 'label> =
  Map.toList >> List.map swap

let private fromContext<'key, 'value, 'label when 'key : comparison> :
            Context<'key, 'value, 'label> -> MapContext<'key, 'value, 'label> =
  fun (pred, _, value, succ) -> fromAdjecent pred, value, fromAdjecent succ

let private toContext<'key, 'value, 'label when 'key : comparison> key :
            MapContext<'key, 'value, 'label> -> Context<'key, 'value, 'label> =
  fun (pred, value, succ) -> toAdjecent pred, key, value, toAdjecent succ

//==== Constructions.
type Id<'value> = 'value -> 'value

let private empt : Graph<'key, 'value, 'label> =
  Map.empty

let private composeGraph context key preds succs =
  let mapAddSucc =
    List.fold (fun graph (value, keyP) ->
              (Optic.map (Map.key_ keyP >?> mapSucc) (Map.add key value)) graph)
  let mapAddPred =
    List.fold (fun graph (value, keyP) ->
              (Optic.map (Map.key_ keyP >?> mapPred) (Map.add key value)) graph)
  Optic.set (Map.value_ key) (Some (fromContext context))
  >> flip mapAddSucc preds
  >> flip mapAddPred succs

let private compose (context : Context<'key, 'value, 'label>) : Id<Graph<'key, 'value, 'label>> =
  composeGraph context
               (Optic.get nodekey context)
               (Optic.get predecessor context)
               (Optic.get successor context)

//==== Deconstruction
let private decomposeContext key =
     Optic.map mapPred (Map.remove key)
  >> Optic.map mapSucc (Map.remove key)
  >> toContext key

let private decomposeGraph key preds succs =
  let foldRemoveSucc =
    List.fold (fun graph (_, anotherKey) -> (Optic.map (Map.key_ anotherKey >?> mapSucc)
                                                       (Map.remove key)) graph)
  let foldRemovePred =
    List.fold (fun graph (_, anotherKey) -> (Optic.map (Map.key_ anotherKey >?> mapPred)
                                                       (Map.remove key)) graph)
  Map.remove key
  >> flip foldRemoveSucc preds
  >> flip foldRemovePred succs

let private decomposeSpecific key (graph : Graph<'key, 'value, 'label>) =
  match Map.tryFind key graph with
  | Some mapContext ->
    let context = decomposeContext key mapContext
    let graph   = decomposeGraph key
                                 (Optic.get predecessor context)
                                 (Optic.get successor context)
                                 graph
    Some context, graph
  | None ->
    None, graph

let private decompose (graph : Graph<'key, 'value, 'label>) :
                      Context<'key, 'value, 'label> option * Graph<'key, 'value, 'label> =
  match Map.tryFindKey (fun _ _ -> true) graph with
  | Some key -> decomposeSpecific key graph
  | None -> None, graph

let private isEmpty<'key, 'value, 'label when 'key : comparison> :
            Graph<'key, 'value, 'label> -> bool =
  Map.isEmpty

//==== Functions.
let rec private ufold f u =
  decompose
  >> function | Some context, graph -> f context (ufold f u graph)
              | _ -> u

let private fold f xs : Graph<'key, 'value, 'label> -> Graph<'key, 'value, 'label> =
  flip (List.fold (flip f)) xs

[<RequireQualifiedAccessAttribute>]
module Graph =

  [<RequireQualifiedAccessAttribute>]
  module Edges =

    let add ((source, destination, label) : LEdge<'key, 'label>) =
         (Optic.map (Map.key_ source >?> mapSucc) (Map.add destination label))
      >> (Optic.map (Map.key_ destination >?> mapPred) (Map.add source label))

    let addMany edges =
      fold add edges

    let remove ((source, destination) : Edge<'key>) =
      decomposeSpecific source
      >> function | Some (pred, key, value, succ), graph ->
                      compose (pred, key, value, List.filter (fun (_, keyP) ->
                                                                keyP <> destination) succ) graph
                  | _, g -> g

    let removeMany edges =
      fold remove edges

    let count<'key, 'value, 'label when 'key : comparison> : Graph<'key, 'value, 'label> -> int =
         Map.toArray
      >> Array.map (fun (_, (_, _, succ)) -> (Map.toList >> List.length) succ)
      >> Array.sum

    let map mapping : Graph<'key, 'value, 'label> -> Graph<'key, 'value, 'newlabel> =
      Map.map (fun key (pred, value, succ) ->
                        Map.map (fun keyP label -> mapping keyP key label) pred,
                        value,
                        Map.map (fun keyP label -> mapping key keyP label) succ)

    let toList<'key, 'value, 'label when 'key : comparison> : Graph<'key, 'value, 'label> -> LEdge<'key, 'label> list =
         Map.toList
      >> List.map (fun (key, (_, _, succ)) ->
                        (Map.toList >> List.map (fun (keyP, label) -> key, keyP, label)) succ)
      >> List.concat

    let contains source destination : Graph<'key, 'value, 'label> -> bool =
         Map.tryFind source
      >> Option.bind (fun (_, _, succ) -> Map.tryFind destination succ)
      >> Option.isSome

    let tryFind source destination : Graph<'key, 'value, 'label> -> LEdge<'key, 'label> option =
         Map.tryFind source
      >> Option.bind (fun (_, _, succ) -> Map.tryFind destination succ)
      >> Option.map (fun label -> (source, destination, label))

    let find source destination =
         tryFind source destination
      >> function | Some label -> label
                  | _ -> failwith (sprintf "Edge %A %A not found" source destination)

  [<RequireQualifiedAccessAttribute>]
  module Nodes =

    let add ((key, value): LNode<'key, 'value>) =
      Map.add key (Map.empty, value, Map.empty)

    let addMany nodes =
      fold add nodes

    let remove key =
         decomposeSpecific key
      >> snd

    let removeMany keys =
      fold remove keys

    let count<'key, 'value, 'label when 'key : comparison> : Graph<'key, 'value, 'label> -> int =
         Map.toArray
      >> Array.length

    let map mapping : Graph<'key, 'value, 'label> -> Graph<'key, 'newval, 'label> =
      Map.map (fun key (pred, value, succ) ->
                    pred, mapping key value, succ)

    let mapFold mapping state : Graph<'key, 'value, 'label> -> 's * Graph<'key, 'newval, 'label> =
         Map.toList
      >> List.mapFold (fun state (key, (pred, value, succ)) ->
                           mapping state key value
                           |> fun (newval, state) -> (key, (pred, newval, succ)), state) state
      >> fun (graph, state) -> state, Map.ofList graph

    let toList<'key, 'value, 'label when 'key : comparison> :
              Graph<'key, 'value, 'label> -> LNode<'key, 'value> list =
         Map.toList
      >> List.map (fun (key, (_, value, _)) -> key, value)

    let contains key : Graph<'key, 'value, 'label> -> bool =
      Map.containsKey key

    let tryFind key : Graph<'key, 'value, 'label> -> LNode<'key, 'value> option =
         Map.tryFind key
      >> Option.map (fun (_, value, _) -> key, value)

    let find key =
      tryFind key
      >> function | Some node -> node
                  | None -> failwith (sprintf "Node %A not found" key)

    let neigbours key =
         Map.tryFind key
      >> Option.map (fun (pred, _, succ) -> Map.toList pred @ Map.toList succ)

    let predecessors key =
         Map.tryFind key
      >> Option.map (fun (pred, _, _) -> Map.toList pred)

    let successors key =
         Map.tryFind key
      >> Option.map (fun (_, _, succ) -> Map.toList succ)

    let outward key =
         Map.tryFind key
      >> Option.map (fun (_, _, succ) -> (Map.toList >> List.map (fun (keyP, label) -> key, keyP, label)) succ)

    let inward key =
         Map.tryFind key
      >> Option.map (fun (pred, _, _) -> (Map.toList >> List.map (fun (keyP, label) -> keyP, key, label)) pred)

    let degree key =
      Map.tryFind key
      >> Option.map (fun (pred, _, succ) -> (Map.toList >> List.length) pred + (Map.toList >> List.length) succ )

    let inwardDegree key =
      Map.tryFind key
      >> Option.map (fun (pred, _, _) -> (Map.toList >> List.length) pred)

    let outwardDegree key =
      Map.tryFind key
      >> Option.map (fun (_, _, succ) -> (Map.toList >> List.length) succ )

  let create nodes edges : Graph<'key, 'value, 'label> =
    (Nodes.addMany nodes >> Edges.addMany edges) empt

  let empty : Graph<'key, 'value, 'label> =
    empt

  let isEmpty<'key, 'value, 'label when 'key : comparison> : Graph<'key, 'value, 'label> -> bool =
    isEmpty

  let map f : Graph<'key, 'value, 'label> -> Graph<'key, 'newval, 'label> =
    Map.map (fun key mapContext -> (toContext key >> f >> fromContext) mapContext)

  let rev<'key, 'value, 'label when 'key : comparison> : Graph<'key, 'value, 'label> -> Graph<'key, 'value, 'label> =
    Map.map (fun _ (pred, value, succ) -> (succ, value, pred))
