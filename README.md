# Stasiun

Should be something like

```
/-------------------------------------------\
| start  (decision)                         |
|   x------>o-----n---->O----------->O      |
|           |       (anotherproc)    |      |
|           y                        |      |
|           |                        |      |
|           v                        v      |
|           O---------->O----------->O      |
|         (proc)    (anothProc)   (another) |
|                                    |      |
|                                    v      |
|                                   fin     |
\-------------------------------------------/
```
