module Tests

open System
open Xunit

open Stasiun.Hekate

let graph1 =
  Graph.empty

let graph2 =
  Graph.create
    [ 'a', "satu"
      'b', "dua"
      'c', "tiga" ]
    [ 'b', 'a', "kiri"
      'c', 'a', "atas"
      'a', 'b', "kanan"
      'b', 'c', "turun" ]

[<Fact>]
let NodesAddBehavesCorrectly () =
  let graph3 = Graph.Nodes.add ('d', "empat") graph2
  Assert.Equal (4, Graph.Edges.count graph3)
  Assert.Equal (4, Graph.Nodes.count graph3)

[<Fact>]
let NodesRemoveBehavesCorrectly () =
  let graph3 = Graph.Nodes.remove 'a' graph2
  Assert.Equal (2, Graph.Nodes.count graph3)
  Assert.Equal (1, Graph.Edges.count graph3)

[<Fact>]
let EdgesAddBehavesCorrectly () =
  let graph3 = Graph.Edges.add ('a', 'c', "bawah") graph2
  Assert.Equal (3, Graph.Nodes.count graph3)
  Assert.Equal (5, Graph.Edges.count graph3)

[<Fact>]
let EdgesRemoveBehavesCorrectly () =
  let graph3 = Graph.Edges.remove ('b', 'a') graph2
  Assert.Equal (3, Graph.Nodes.count graph3)
  Assert.Equal (3, Graph.Edges.count graph3)

[<Fact>]
let EdgesContainsBehavesCorrectly () =
  Assert.True (Graph.Edges.contains 'a' 'b' graph2)
  Assert.False (Graph.Edges.contains 'a' 'c' graph2)

[<Fact>]
let NodesContainsBehavesCorrectly () =
  Assert.True (Graph.Nodes.contains 'a' graph2)
  Assert.False (Graph.Nodes.contains 'd' graph2)

[<Fact>]
let IsEmptyBehavesCorrectly () =
  Assert.True (Graph.isEmpty graph1)
  Assert.False (Graph.isEmpty graph2)

[<Fact>]
let EdgesMapBehavesCorrectly () =
  let graph3 = Graph.Edges.map (fun keyA keyB value -> sprintf "%c.%c.%s" keyA keyB value) graph2
  Assert.Equal (('a', 'b', "a.b.kanan"), Graph.Edges.find 'a' 'b' graph3)

[<Fact>]
let NodesMapBehavesCorrectly () =
  let graph3 = Graph.Nodes.map (fun key (node : string) -> node.ToUpper()) graph2
  Assert.Equal ("satu", snd (Graph.Nodes.find 'a' graph2))
  Assert.Equal ("SATU", snd (Graph.Nodes.find 'a' graph3))

[<Fact>]
let NodesMapFoldBehavesCorrectly () =
  let state, graph3 = Graph.Nodes.mapFold (fun state _ (node : string) -> node.ToUpper(), state + 1) 0 graph2
  Assert.Equal ("satu", snd (Graph.Nodes.find 'a' graph2))
  Assert.Equal ("SATU", snd (Graph.Nodes.find 'a' graph3))
  Assert.Equal (3, state)

[<Fact>]
let NodesToListBehavesCorrectly () =
  Assert.Equal (3, Graph.Nodes.toList graph2 |> List.length)

[<Fact>]
let EdgesToListBehavesCorrectly () =
  Assert.Equal (4, Graph.Edges.toList graph2 |> List.length)

[<Fact>]
let NodesTryFindBehavesCorrectly () =
  Assert.Equal (Some ('a', "satu"), Graph.Nodes.tryFind 'a' graph2)
  Assert.Equal (None, Graph.Nodes.tryFind 'a' graph1)

[<Fact>]
let NodesNeighboursBehavesCorrectly () =
  Assert.Equal (Some [ 'b', "kiri"; 'c', "atas"; 'b', "kanan" ], Graph.Nodes.neigbours 'a' graph2)

[<Fact>]
let NodesSuccessorsBehavesCorrectly () =
  Assert.Equal (Some [ 'b', "kanan" ], Graph.Nodes.successors 'a' graph2)

[<Fact>]
let NodesPredecessorsBehavesCorrectly () =
  Assert.Equal (Some [ 'b', "kiri"; 'c', "atas" ], Graph.Nodes.predecessors 'a' graph2)

[<Fact>]
let NodesOutwardBehavesCorrectly () =
  Assert.Equal (Some [ 'a', 'b', "kanan" ], Graph.Nodes.outward 'a' graph2)

[<Fact>]
let NodesInwardBehavesCorrectly () =
  Assert.Equal (Some [ 'b', 'a', "kiri"; 'c', 'a', "atas" ], Graph.Nodes.inward 'a' graph2)

[<Fact>]
let NodesDegreeBehavesCorrectly () =
  Assert.Equal (Some 3, Graph.Nodes.degree 'a' graph2)

[<Fact>]
let NodesInwardDegreesBehavesCorrectly () =
  Assert.Equal (Some 2, Graph.Nodes.inwardDegree 'a' graph2)

[<Fact>]
let NodesOutwardDegreesBehavesCorrectly () =
  Assert.Equal (Some 1, Graph.Nodes.outwardDegree 'a' graph2)
